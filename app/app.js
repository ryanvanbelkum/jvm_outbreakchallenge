"use strict";

let express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bodyParser = require('body-parser'),
    date = new Date();

//SETUP MONGO
mongoose.connect('mongodb://localhost/infectionDB');
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('we are connected!');
});

let diagnosis = new Schema({
   user: String,
   date: Date,
   decision: String
});
let diagnosisModel = mongoose.model('Diagnosis', diagnosis);

//DR. ENUM
let doc = {
    'RE': 1,
    'RV': 2,
    'JH': 3,
    'MY': 4,
    'NS': 5,
    'PP': 6,
    'ZB': 7
    };

// this will let us get the data from a POST
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// ROUTES FOR OUR API
// =============================================================================
let router = express.Router(); // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function (req, res) {
    res.json({
        message: 'hooray! welcome to our api!'
    });
});

// more routes for our API will happen here
router.post('/decision', function (req, res) {
    let desc = req.body;
    desc.data = getTime();
    console.log(JSON.stringify(desc));

    if (!(desc.user in doc)){
        res.status(404).send('invalid user');
        console.log('invalid user');
        return;
    }

    let diagnosis = new diagnosisModel({
       name: desc.user,
       date: getTime(),
       decision: desc.diagnosis
    });

    diagnosis.save(function(err, diag) {
       if (err) return console.error(err);
        res.status(200).send(diag);
    });

});


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);


//support functions
function getTime() {
    return date.getTime();
}

app.listen(process.env.PORT || 8080);
console.log("the server has started!");
